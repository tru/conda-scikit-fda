## Installing scikit-fda in a conda environment (python 3.9)
Let's take another real world example.

## Requirements:
- conda installed

## Method
- Use a sand box env until the recipe is finished
- only use pip as the last step when no conda packages are found

## Howto
```
# install a minimal python-3.9 
conda create -n py39 && conda activate py39 && conda install python=3.9
# https://github.com/GAA-UAM/scikit-fda requires fdasrsf 
# installing fdasrsf first from
# https://github.com/jdtuck/fdasrsf_python
conda install -c conda-forge fdasrsf
# let's install scikit-fda and see what it pulls
pip install scikit-fda
conda env export | grep -A100 pip:
conda env export > ~/environment.yml
# we can install more conda packages before going with pip
# rince and repeat
```

## Documenting as we progress: 
- adding the environment.yml to the project and improving it at each stage.

## Rince and repeat
```
(py39) [tru@sillage ~]$ conda deactivate
# clean-up
(base) [tru@sillage ~]$ conda env remove -n py39
# edit ~/environment.yml to remove the pip: section
(base) [tru@sillage ~]$ conda env create -n py39 --file /tmp/environment.yml
(base) [tru@sillage ~]$ conda activate py39
(py39) [tru@sillage ~]$ conda add pands pytz scikit-learn sympy xarray
<...>
# save 
(py39) [tru@sillage ~]$ conda env export > ~/environment.yml
# keep adding as long as conda finds the packages
(py39) [tru@sillage ~]$ conda install dcor rdata
# save 
(py39) [tru@sillage ~]$ conda env export > ~/environment.yml
(py39) [tru@sillage ~]$ pip3 install scikit-fda
Collecting scikit-fda
<...>
Installing collected packages: scikit-datasets, typing-extensions, multimethod, scikit-fda
Successfully installed multimethod-1.6 scikit-datasets-0.1.38 scikit-fda-0.6 typing-extensions-3.10.0.2
(py39) [tru@sillage ~]$ conda env export > /home/tru/git/gitlab.pasteur.fr/conda-scikit-fda/environment.yml
```

## conda search

You can also use `conda search xxx` to help finding the right packages.
```
(py39) [tru@sillage ~]$ conda search multimethod 
Loading channels: done
# Name                       Version           Build  Channel             
multimethod                      1.3            py_0  conda-forge         
multimethod                      1.4            py_0  conda-forge         
multimethod                      1.4    pyhd3eb1b0_0  pkgs/main           
multimethod                      1.5    pyhd3eb1b0_0  pkgs/main           
multimethod                      1.5    pyhd8ed1ab_0  conda-forge         
multimethod                      1.6    pyhd8ed1ab_0  conda-forge         
(py39) [tru@sillage ~]$ conda search scikit-datasets
Loading channels: done
No match found for: scikit-datasets. Search: *scikit-datasets*

PackagesNotFoundError: The following packages are not available from current channels:

  - scikit-datasets

Current channels:

  - https://conda.anaconda.org/conda-forge/linux-64
  - https://conda.anaconda.org/conda-forge/noarch
  - https://repo.anaconda.com/pkgs/main/linux-64
  - https://repo.anaconda.com/pkgs/main/noarch
  - https://repo.anaconda.com/pkgs/r/linux-64
  - https://repo.anaconda.com/pkgs/r/noarch

To search for alternate channels that may provide the conda package you're
looking for, navigate to

    https://anaconda.org

and use the search bar at the top of the page.


(py39) [tru@sillage ~]$ 
```
We can `conda install multimethod typing-extensions` and use pip for the last stage.

## Finally

```
(py39) [tru@sillage ~]$ pip3 install scikit-fda
Collecting scikit-fda
  Using cached scikit_fda-0.6-py2.py3-none-any.whl (339 kB)
Requirement already satisfied: matplotlib in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (3.4.3)
Requirement already satisfied: typing-extensions in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (3.10.0.2)
Requirement already satisfied: cython in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (0.29.24)
Requirement already satisfied: multimethod>=1.5 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (1.6)
Requirement already satisfied: pandas in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (1.3.3)
Requirement already satisfied: scikit-learn>=0.20 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (1.0)
Requirement already satisfied: dcor in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (0.5.3)
Requirement already satisfied: scipy>=1.3.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (1.7.1)
Requirement already satisfied: findiff in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (0.8.9)
Requirement already satisfied: numpy>=1.16 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (1.21.2)
Requirement already satisfied: fdasrsf>=2.2.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (2.3.3)
Requirement already satisfied: rdata in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-fda) (0.4)
Collecting scikit-datasets[cran]>=0.1.24
  Using cached scikit_datasets-0.1.38-py3-none-any.whl (30 kB)
Requirement already satisfied: tqdm in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (4.62.3)
Requirement already satisfied: pyparsing in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (2.4.7)
Requirement already satisfied: patsy in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (0.5.2)
Requirement already satisfied: six in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (1.16.0)
Requirement already satisfied: GPy in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (1.10.0)
Requirement already satisfied: joblib in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (1.0.1)
Requirement already satisfied: numba in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (0.53.1)
Requirement already satisfied: cffi>=1.0.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from fdasrsf>=2.2.0->scikit-fda) (1.14.6)
Requirement already satisfied: pycparser in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from cffi>=1.0.0->fdasrsf>=2.2.0->scikit-fda) (2.20)
Requirement already satisfied: threadpoolctl>=2.0.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from scikit-learn>=0.20->scikit-fda) (2.2.0)
Requirement already satisfied: llvmlite<0.37,>=0.36.0rc1 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from numba->fdasrsf>=2.2.0->scikit-fda) (0.36.0)
Requirement already satisfied: setuptools in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from numba->fdasrsf>=2.2.0->scikit-fda) (58.0.4)
Requirement already satisfied: sympy in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from findiff->scikit-fda) (1.8)
Requirement already satisfied: paramz>=0.9.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from GPy->fdasrsf>=2.2.0->scikit-fda) (0.9.5)
Requirement already satisfied: decorator>=4.0.10 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from paramz>=0.9.0->GPy->fdasrsf>=2.2.0->scikit-fda) (5.1.0)
Requirement already satisfied: cycler>=0.10 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from matplotlib->scikit-fda) (0.10.0)
Requirement already satisfied: pillow>=6.2.0 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from matplotlib->scikit-fda) (8.3.2)
Requirement already satisfied: python-dateutil>=2.7 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from matplotlib->scikit-fda) (2.8.2)
Requirement already satisfied: kiwisolver>=1.0.1 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from matplotlib->scikit-fda) (1.3.2)
Requirement already satisfied: pytz>=2017.3 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from pandas->scikit-fda) (2021.1)
Requirement already satisfied: xarray in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from rdata->scikit-fda) (0.19.0)
Requirement already satisfied: mpmath>=0.19 in /c7/home/tru/miniconda3/envs/py39/lib/python3.9/site-packages (from sympy->findiff->scikit-fda) (1.2.1)
Installing collected packages: scikit-datasets, scikit-fda
Successfully installed scikit-datasets-0.1.38 scikit-fda-0.6
(py39) [tru@sillage ~]$ conda env export > environment.yml
# uploaded to this repository
```

## Memory lane

How do I read the modification on the environment.yml file?
(gilatb history does not seem to allow to only track one single file).

- use `git log` if you are familiar with `diff` output
```
$ git log --follow -p  environment.yml

```
- use `tig` (https://github.com/jonas/tig/) available as module on our /c7/shared  
```
$ module add tig 
$ tig log environment.yml
```
